@extends('layout.main')

@section('title', 'Halaman Home')

@section('content')
<form action="{{route('welcome')}}" method="GET">
    <h1>Buat Account Baru</h1>

    <h3>Sign Up Form</h3>

    <label>First name :</label><br/>
    <input type="text" name="first_name" /><br/><br/>

    <label>Last name :</label><br/>
    <input type="text" name="last_name" /><br/><br/>

    <label>Gender</label><br/>
    <input type="radio" value="male" name="gender"/> Male<br/>
    <input type="radio" value="female" name="gender"/> Female<br/><br/>

    <label>Nationality</label><br/>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="foreign">Foreign</option>
    </select><br/><br/>

    <label>Language Spoken</label><br/>
    <input type="checkbox" name="language" value="bahasa"> Bahasa Indonesia<br/>
    <input type="checkbox" name="language" value="english">
    English<br/>
    <input type="checkbox" name="language" value="other"> Other<br/><br/>

    <label>Bio</label><br/>
    <textarea name="bio"></textarea><br/><br/>

    <input type="submit" value="Sign Up"/>
</form>
@endsection