@extends('layout.main')

@section('title', 'Halaman Home')

@section('content')
  <h1>SELAMAT DATANG! {{$data['first_name']}} {{$data['last_name']}}</h1>
  <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection