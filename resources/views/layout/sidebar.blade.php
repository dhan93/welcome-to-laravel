@php
    $sidebarMenus = [
      [
        'title' => 'Dashboard',
        'link' => route('home'),
        'icon' => 'fas fa-tachometer-alt',
        'child' => []
      ],
      [
        'title' => 'Table',
        'link' => '#',
        'icon' => 'fas fa-th',
        'child' => [
          [
            'title' => 'Data Table',
            'link' => route('table'),
            'icon' => 'far fa-circle',
          ],
        ]
      ],
      [
        'title' => 'Pemain',
        'link' => '#',
        'icon' => 'far fa-meh',
        'child' => [
          [
            'title' => 'Data Pemain',
            'link' => route('cast.index'),
            'icon' => 'far fa-circle',
          ],
          [
            'title' => 'Tambah Pemain',
            'link' => route('cast.create'),
            'icon' => 'fas fa-plus',
          ],
        ]
      ],
    ]
@endphp

<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="{{asset('img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="pb-3 mt-3 mb-3 user-panel d-flex">
      <div class="image">
        <img src="{{asset('img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        
        @foreach ($sidebarMenus as $menu)     
          <li class="nav-item">
            <a href="{{$menu['link']}}" class="nav-link">
              <i class="nav-icon {{$menu['icon']}}"></i>
              <p>
                {{$menu['title']}}
                @if (count($menu['child']))
                  <i class="right fas fa-angle-left"></i>    
                @endif
              </p>
            </a>
            @if (count($menu['child']))
              <ul class="nav nav-treeview">
                @foreach ($menu['child'] as $child)
                    <li class="nav-item">
                      <a href="{{$child['link']}}" class="nav-link">
                        <i class="nav-icon {{$child['icon']}}"></i>
                        <p>{{$child['title']}}</p>
                      </a>
                    </li>                  
                @endforeach
              </ul>
            @endif
          </li>
        @endforeach
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>