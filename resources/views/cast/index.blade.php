@extends('layout.main')

@section('title', 'Daftar Pemain')

@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('card-tools')
  <a class="btn btn-success btn-sm" href="{{route('cast.create')}}" role="button">
    <i class="fas fa-plus"></i> Tambah Pemain
  </a>
@endsection

@push('scripts')
  <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@section('content')
  @if (!count($casts))
    <div class="text-center d-block">
      Belum ada data. <a href="{{route('cast.create')}}">Tambahkan data sekarang!</a>
    </div>
  @else
    <div class="row">
      <div class="col-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($casts as $cast)
              <tr>
                <td>{{$loop->index+1}}</td>
                <td>{{$cast->nama}}</td>
                <td>{{$cast->umur}}</td>
                <td class="text-center">
                  <a class="btn btn-primary" href="{{route('cast.show', $cast->id)}}" role="button">
                    <i class="fas fa-eye"></i> detail
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  @endif

@endsection