@extends('layout.main')

@section('title', $title)

@section('content')
  <div class="row justify-content-center">
    <div class="col-md-6 col-12">

    @if (isset($cast))
      <form action="{{route('cast.update', $cast->id)}}" method="post">
      @method('PUT')
    @else
      <form action="{{route('cast.store')}}" method="post">
    @endif

        @if ($errors->any())
          <div class="alert alert-danger" role="alert">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        @csrf
        <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control @error('nama') is-invalid @enderror" id="name" name="nama" value="{{$cast->nama??old('nama')}}" required>
          @error('nama')
            <div id="" class="invalid-feedback">
              {{$message}}
            </div>
          @enderror
        </div>

        <div class="form-group">
          <label for="age">umur</label>
          <input type="number" class="form-control @error('title') is-invalid @enderror" id="age" name="umur" min="0" step="1" value="{{$cast->umur??old('nama')}}" required>
          @error('umur')
            <div id="" class="invalid-feedback">
              {{$message}}
            </div>
          @enderror
        </div>

        <div class="form-group">
          <label for="bio">Example textarea</label>
          <textarea class="form-control @error('title') is-invalid @enderror" id="bio" rows="3" name="bio" required>{{$cast->bio??old('nama')}}</textarea>
          @error('bio')
            <div id="" class="invalid-feedback">
              {{$message}}
            </div>
          @enderror
        </div>

        @if (isset($cast))
          <a role="button" href="{{route('cast.show', $cast->id)}}" class="btn btn-secondary">
            <i class="fas fa-arrow-left"></i> Batal
          </a>
          <button type="submit" class="btn btn-warning">
            <i class="fas fa-edit"></i> Update
          </button>
        @else
          <button type="submit" class="btn btn-primary">
            <i class="fas fa-save"></i> Simpan
          </button>
        @endif   
      </form>
    </div>
  </div>
@endsection