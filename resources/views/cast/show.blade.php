@extends('layout.main')

@section('title', 'Detail Pemain')

@section('content')
  <div class="row">
    <div class="col-12">
      <table class="table table-borderless">
        <tbody>
          <tr>
            <td class="col-auto font-weight-bold">Nama</td>
            <td class="col-auto">:</td>
            <td class="col">{{$cast->nama}}</td>
          </tr>
          <tr>
            <td class="font-weight-bold">Umur</td>
            <td>:</td>
            <td>{{$cast->umur}}</td>
          </tr>
          <tr>
            <td class="font-weight-bold">Bio</td>
            <td>:</td>
            <td>{{$cast->bio}}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-12 text-right">
      <a href="{{route('cast.edit', $cast->id)}}" role="button" class="btn btn-warning">
        <i class="fas fa-edit"></i> Edit
      </a>
      <button type="button" class="btn btn-danger ml-2" data-toggle="modal" data-target="#exampleModal">
        <i class="fas fa-trash"></i> Hapus
      </button>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header border-0">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
          Hapus data pemain <strong>{{$cast->nama}}</strong>?
        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <i class="fas fa-ban"></i> Batal
          </button>
          <form action="{{route('cast.destroy', $cast->id)}}" method="POST">
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger" role="button">
              <i class="fas fa-trash"></i> Hapus
            </a>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection