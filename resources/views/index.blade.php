@extends('layout.main')

@section('title', 'Halaman Home')

@section('content')
  <h1>Media Online</h1>
  <h2>Sosial Media Developer</h2>
  <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
  <h3>Benefit join di Media Online</h3>
  <ul>
    <li>Mendapatkan motovasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon developer terbaik</li>
  </ul>
  <h3>Cara bergabung ke Media Online</h3>
  <ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftar di <a href="{{route('register')}}">Form Sign Up</a></li>
    <li>Selesai</li>
  </ol>
@endsection