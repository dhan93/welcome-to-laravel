<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $casts = Cast::all();
      return view('cast.index', compact('casts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('cast.create', ['title' => 'Tambah Pemain']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validated = $this->validate($request,[
    		'nama' => 'required|string',
    		'umur' => 'required|integer|min:0',
        'bio' => 'required|string'
    	]);
      
      Cast::create($validated);

      return redirect(route('cast.index'))->with('success', 'data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $cast = Cast::find($id);

      return view('cast.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $cast = Cast::find($id);
      $title = 'Edit Pemain';
      return view('cast.create', compact('cast', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validated = $this->validate($request,[
    		'nama' => 'required|string',
    		'umur' => 'required|integer|min:0',
        'bio' => 'required|string'
    	]);

      $cast = Cast::find($id);
      $cast->nama = $validated['nama'];
      $cast->umur = $validated['umur'];
      $cast->bio = $validated['bio'];

      $cast->update();

      return redirect(route('cast.show', $id))->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $cast = Cast::find($id);
      $cast->delete();

      return redirect(route('cast.index'))->with('success', 'Data berhasil dihapus');
    }
}
