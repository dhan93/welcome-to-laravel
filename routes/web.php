<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home')->name('home');
Route::get('data-tables', 'IndexController@dataTable')->name('table');

Route::get('register', 'AuthController@register')->name('register');
Route::get('welcome', 'AuthController@welcome')->name('welcome');

Route::resource('cast', CastController::class);